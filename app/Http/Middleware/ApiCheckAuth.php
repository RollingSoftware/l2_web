<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;

class ApiCheckAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (!Auth::guard($guard)->check()) {
            $data = [
                'code' => 401,
                'data' => 'Unauthorized'
            ];
            return response()->json($data, Response::HTTP_OK);
        }
        
        if(Auth::user()['accessLevel'] < 0) {
            Auth::logout();
            $data = [
                'code' => 403,
                'data' => 'User is Blocked'
            ];
            return response()->json($data, Response::HTTP_OK);
        }

        return $next($request);
    }
}
