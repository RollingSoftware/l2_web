<?php
/**
 * Created by PhpStorm.
 * User: Morris
 * Date: 4/30/18
 * Time: 12:09 AM
 */

namespace App\Services\Api\Server;




use App\Models\Game\Character;
use App\Repositories\Api\ServerRepository;
use App\Repositories\Game\CharacterRepository;

class ServerService
{

    CONST ONLINE_ADJUSTER = 1.7;
    CONST STATIC_ONLINE = 100;

    private $serverRepository;
    private $characterRepository;

    public function __construct(ServerRepository $serverRepository, CharacterRepository $characterRepository)
    {
        $this->serverRepository = $serverRepository;
        $this->characterRepository = $characterRepository;
    }

    public function online() {
        if ($this->serverStatusCheck(env('GAME_SERVER_IP'), env('GAME_SERVER_PORT')) == 'Online') {
            return round($this->serverRepository->countCharacters(Character::ONLINE) * self::ONLINE_ADJUSTER + self::STATIC_ONLINE, 0, PHP_ROUND_HALF_UP);
        } else {
            return 0;
        }
    }

    public function info()
    {
        $offlineTradePlayers = $this->serverRepository->countCharacters(Character::OFFLINE_TRADE);

        $serverStatus = $this->serverStatusCheck(env('GAME_SERVER_IP'), env('GAME_SERVER_PORT'));

        return ['online_count' => $this->online(), 'offline_traders_count' => $offlineTradePlayers, 'server' => $serverStatus];
    }

    private function serverStatusCheck($host, $port)
    {
        $waitTimeoutInSeconds = 1;
        try {
            $server_check_result = fsockopen($host, $port, $errCode, $errStr, $waitTimeoutInSeconds);
            if ($server_check_result) {
                return 'Online';
            } else {
                return 'Offline';
            }
        } catch (\Exception $e) {
            return 'Offline';
        }
    }

    public function currentOnlineFile()
    {
        return $this->online();
    }


    public function unstuck($user, $id)
    {
        if ($user == null) {
            return 'No user';
        }

        $character = $this->characterRepository->findByAccoundAndId($user, $id);
        if (empty($character) || $character == null) {
            return 'Fuck you';
        }

        if ($character->get('online') !== 0)
        {
            return "Character has to be offline";
        }

        $x = 83498 + rand(-30, 30);
        $y = 148621 + rand(-30, 30);

        $data = [
            'x' => $x,
            'y' => $y,
            'z' => -3400,
            'transform_id' => 0,
            'isin7sdungeon' => 0
        ];

        $this->characterRepository->updateById($id, $data);

        return true;
    }
}