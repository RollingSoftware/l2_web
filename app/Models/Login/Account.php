<?php

namespace App\Models\Login;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class Account extends Authenticatable
{

    CONST ACTIVE = 0;
    CONST BLOCKED = -1;

    use Notifiable;

    protected $connection = 'login';

    public $incrementing = false;

    public $timestamps = false;
    public $table = "accounts";

    protected $primaryKey = 'login';

    protected $fillable = ['login', 'password', 'email', 'accessLevel', 'language'];

    public static function passwordHash ($password, $options = [])
    {
        $result = bcrypt($password, $options);

        if ($result[2] == 'y') {
            $result[2] = 'a';
        }

        return $result;
    }


}
