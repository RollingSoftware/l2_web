<?php

namespace App\Models\Web;


use Illuminate\Database\Eloquent\Model;


class RegistrationToken extends Model
{

    CONST ACTIVE = 0;
    CONST USED = 1;
    CONST TOKEN_VALID = 30*60; 

    protected $connection = 'web';

    public $timestamps = false;
    public $table = "registration_token";

    protected $fillable = ['token', 'expire_date', 'status', 'username'];

}
