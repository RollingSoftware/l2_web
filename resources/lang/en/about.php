<?php

return [

  'about' => '
<p id="summary">
Mobs are stronger, ALT+B buffer is free until 2nd profession change!</br>
</br>
Mobs drop everything, from items, parts and recipes, to attributes stones and giant codex</br>
</br>
High rate Raid Boss drops, actual drop worth fighting for!</br>
</br>
You have a chance to drop your items in pvp, sieges and just deaths from mobs!</br>
</br>
Subskills are replaced with Mastery skills, check with Village Masters about them!</br>
Subclasses can reach 85 level and register for olympiad, can be played as a main class!</br>
</br>
Most instances are removed to motivate open world battles!</br>
Hellbound and Gracia are removed for the same reason!</br>
</br>
Everyone can craft!</br>
Drop and Craft has random 1-16 enchant chance!</br>
Monsters have random, powerful skills!</br>
</br>
Pets are sold at Pet Managers!</br>
</br>
Short access to Grand Bosses, items sold at Mammon Black Marketeers!</br>
</p>

<h3 id="gameplay">Gameplay</h3>
<ul>
  <li>Mana Potions and stuff in Grocery Shops</li>
  <li>Subskills are replaced with Mastery skills</li>
  <li>Professions can be acquired trough quests or bough via adena</li>
  <li>Most transformations are removed</li>
  <li>ALT+B buffer is free until 2nd profession change</li>
  <li>Herb potions are removed</li>
  <li>Death penalty removed</li>
  <li>Cloaks can be worn with A and S grade sets</li>
  <li>Subclass do not require quest completion</li>
  <li>Noblesse are acquired via short quest, started in Goddard from NPC Caradine</li>
</ul>

<h3 id="crafting">Crafting</h3>
<ul>
  <li>Seal/Unseal, SA, Gemstones and Dualcrafts are available in ALT+B</li>
  <li>Special sell/store all recipes or resources trader dialog option</li>
  <li>All manor can be used without level restriction</li>
  <li>Newbie helper/Dimensional Merchant and Item Broker are removed</li>
  <li>Monsters drop all required craft ingredients and even complete items!</li>
  <li>Offline trade - just open trading and exit the game</li>
  <li>All Shadow/Common items are removed</li>
</ul>

<h3 id="pvp">PvP</h3>
<ul>
  <li>Castles and Fortresses have daily resource income</li>
  <li>Short Olympiad Cycle, only 2 weeks</li>
  <li>Castle sieges every week</li>
  <li>NPC Death Drop: 10%</li>
  <li>PvP Death Drop: 15%</li>
  <li>Clan War Death Drop: 20%</li>
  <li>Siege Death Drop: 8%</li>
  <li>PK Death Drop: 80%</li>
</ul>

<p><b>Augmented items can be dropped/traded/sold</b></p>

<h3 id="pve">PvE</h3>
<ul>
  <li>Raid Boss drops are actually worth fighting for</li>
  <li>Regular mobs have good drop chances</li>
  <li>All mobs are 2x stronger</li>
  <li>All town guards are 3x stonger</li>
  <li>All bosses are instanceless</li>
  <li>Kamaloka, Pailaka, Chamber of Delusions and Fantasy Island are removed</li>
</ul>

<h3 id="rates">Rates</h3>
<ul>
  <li><b>Exp:</b> 100x</li>
  <li><b>SP:</b> 20x</li>
  <li><b>Adena:</b> 10x</li>
  <li><b>Manor:</b> 5x</li>
  <li><b>Drop:</b> 30x</li>
  <li><b>Raid:</b> 30x</li>
  <li><b>Spoil:</b> 20x</li>
  <li><b>Quest:</b> 4x</li>
</ul>

<h3 id="cursed_weapons">Cursed weapons</h3>
<ul>
  <li>Drop chance 0.01%</li>
  <li>Disappearance upon death chance 60%</li>
  <li>Weapon life time is 3 hours</li>
  <li>Each player kill decreases the life time by 1 minute</li>
  <li>10 players kills per weapon stage</li>
</ul>

<h3 id="epic_bosses">Epic Bosses</h3>
<ul>
  <li>Valakas: 62 +- 8 (Drops Elegia)</li>
  <li>Antharas: 62 +- 8 (Drops Elegia)</li>
  <li>Freya: 48 +- 8 (Not in an instance, drops Vorpal)</li>
  <li>Frintezza: 48 +- 8 (Not in an instance, drops Vorpal)</li>
  <li>Zaken: 48 +- 8 (Level 85, not in an instance, drops Vorpal)</li>
  <li>Baium: 48 +- 8 (Level 85, drops Vorpal)</li>
  <li>Core: 48 +- 8 (Level 85, drops Vorpal)</li>
  <li>Orfen: 48 +- 8 (Level 85, drops Vorpal)</li>
  <li>Queen Ant: 36 +- 8 (Level 85, drops Vorpal)</li>
</ul>

<hr/>
'



];