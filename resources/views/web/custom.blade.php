@extends('index')

@section('content')

    <h2 class="center-text">{{trans('Advertising Stats')}}</h2>
    <hr/>

    @if ($data->isEmpty())
    <h3 class="center-text">{{trans('No statistics available')}}</h3>
    @else

    <div class="article">
       <table>
           <thead>
               <tr>
                   <td>name</td>
                   <td>view</td>
                   <td>unique_view</td>
                   <td>registered</td>
               </tr>
           </thead>
           <tbody>
           @foreach ($data->all() as $stat)
               <tr>
                   <td>
                       {{ $stat->name }}
                   </td>
                   <td>
                       {{ $stat->view  }}
                   </td>
                   <td>
                       {{ $stat->unique_view }}
                   </td>
                   <td>
                       {{ $stat->registered }}
                   </td>
               </tr>
           @endforeach
           </tbody>
       </table>
        <hr/>
    </div>

        <style>
            table {
                margin-left: 30%;
            }
            table, th, td {
                padding: 7px;
                min-width: 25px;
                text-align: center;
                border: 1px solid #c6c6ab;
            }
        </style>

    @endif


@endsection
