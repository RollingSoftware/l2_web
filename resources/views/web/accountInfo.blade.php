@extends('auth.account')

@section('account_content')


   <div class ="accountInfo">
      <p>{{ trans('body.Hello') }}{{ "  " }}{{ Auth::user()->login }}</p>
      <br>
      <table class="characterTable">
         <thead >
         <tr>
            <td>{{ trans('body.Character') }}</td>
            <td>{{ trans('body.pk')}}</td>
            <td>{{ trans('body.pvp')}}</td>
            <td>{{ trans('body.karma') }}</td>
            <td>{{ trans('body.sp')}}</td>
            <td>{{ trans('body.fame') }}</td>
            <td>{{ trans('body.clan') }}</td>
            <td></td>
         </tr>
         </thead>
         <tbody>

         @foreach ($characters->all() as $ch)
         <tr>
            <td>{{ $ch->char_name }}</td>
            <td>{{ $ch->pkkills }}</td>
            <td>{{ $ch->pvpkills }}</td>
            <td>{{ $ch->karma }}</td>
            <td>{{ $ch->sp }}</td>
            <td>{{ $ch->fame }}</td>
            @if($ch->clan && $ch->clan->clan_name)
               <td>{{ $ch->clan->clan_name }}</td>
               @else
               <td></td>
            @endif
            <td> <button type="button" onclick="unstuck({{$ch->charId}})">unstuck</button></td>
         </tr>
         @endforeach
         </tbody>
      </table>
   </div>

   <style>
      .characterTable{
         margin-top: 10px;
         border: 2px solid #dec99ee6;
         width: 100%;
         border-spacing: 7px 11px;
      }

      .characterTable td{
         padding: 4px;
         text-align: center;
      }

      .characterTable tr{
         border: 4px;
      }

      .characterTable a:hover {
         color: cornflowerblue;
      }

      .accountInfo{
         width: 70%;
         padding: 10px;
         margin: auto;
      }

      .accountInfo p {
         text-align: center;
      }
   </style>




@endsection
