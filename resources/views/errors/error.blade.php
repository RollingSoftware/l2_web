@extends('index')

@section('content')

   <div class="error_block">
      <p>{{ trans($message) }}</p>
   </div>

@endsection
