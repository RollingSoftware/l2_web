@extends('auth.account')

@section('account_content')

    <form method="POST" action="{{ route('login', ['locale' => App::getLocale()]) }}">
        {{ csrf_field() }}
        <div class="center-text ">
            <div class="border-pane">

                <label for="login">{{trans('body.Username')}}</label>
                <input id="login" type="text" class="form-control" name="login" value="{{ old('email') }}" required
                       autofocus>
                <label for="password">{{trans('body.Password')}}</label>
                <input id="password" type="password" class="form-control" name="password" required>
                <label>
                    <input type="checkbox"
                           name="remember" {{ old('remember') ? 'checked' : '' }}>{{trans('body.Remember Me')}}
                </label>
            </div>

            <div class="g-recaptcha" data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY') }}"></div>

            <button type="submit">{{trans('body.Login')}}</button>
            <div class='accounts-misc'>
                <p>{{trans('body.active_account')}} <a href="{{ route('reSendActivateLink', ['locale' => App::getLocale()]) }}">{{trans('body.active_account_url')}}</a></p>
                <p>{{trans('body.Forgot Password')}} <a href="{{ route('password.request', ['locale' => App::getLocale()]) }}">{{trans('body.Forgot Password_url')}}</a></p>
            </div>
        </div>
    </form>

@endsection
