<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->get('/', 'web\HomeController@redirect')->middleware('global');

$router->group(['prefix' => '{locale}', 'namespace' => 'web', 'middleware' => 'global'], function () use ($router) {

    $router->get('/verify-user/{code}', 'Auth\RegisterController@activateUser')->name('activate.user');

    $router->get('/send/activate/url', 'Auth\RegisterController@reSendActivateLinkForm')->name('reSendActivateLink');
    $router->post('/send/activate/url', 'Auth\RegisterController@reSendActivateLink')->name('post.reSendActivateLink');

    $router->get('/', 'HomeController@redirect')->middleware('global');
    
    $router->get('/home', 'HomeController@news')->name('home');

    $router->get('/downloads', 'StartPlayingController@index')->name('downloads');
    $router->get('/about', 'AboutController@index')->name('about');
    $router->get('/castles', 'CastleController@index')->name('castles');
    $router->get('/raidbosses', 'RaidBossController@index')->name('raidbosses');

    $router->get('/login', 'AuthenticatesUsers@login')->name('login');

    $router->get('/ch', 'AuthenticatesUsers@login')->name('changepassword');

    $router->group(['middleware' => 'authCheck'], function () use ($router) {
        $router->get('/user/account', 'AccountController@info')->name('account');

        $router->get('/custom/stat', 'CustomController@index')->name('custom');

        $router->get('logout', 'Auth\LoginController@logout')->name('logout');
    });

    $router->get('login', 'Auth\LoginController@showLoginForm')->name('login');

    $router->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    
    $router->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    $router->get('password/change', 'Auth\ChangePwdController@index')->name('get.password.change');

    $router->get('error', 'Error\ErrorController@handler')->name('error');

    $router->group(['middleware' => 'reCaptcha'], function () use ($router) {
        $router->post('login', 'Auth\LoginController@login');
        $router->post('register', 'Auth\RegisterController@register');
        $router->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
        $router->post('password/reset', 'Auth\ResetPasswordController@reset')->name('post.password.reset');
        $router->group(['middleware' => 'authCheck'], function () use ($router) {
            $router->post('password/change', 'Auth\ChangePwdController@change')->name('post.password.change');
        });
    });
});

$router->group(['middleware' => 'apiAuthCheck'], function () use ($router) {
    $router->get('/server/unstuck/{id}', 'api\ServerController@unstuck')->name('server.unstuck');
});

$router->get('password/reset/{token}', 'web\Auth\ResetPasswordController@showResetForm')->name('password.reset');
$router->get('/server/info', 'api\ServerController@info')->name('server.info');
$router->get('/server/online.txt', 'api\ServerController@currentOnlineFile')->name('server.current.online.file');

if (env('APP_ENV') === 'prod') {
    URL::forceScheme('https');
}